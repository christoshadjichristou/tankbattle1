using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeadState : FSMState
{
    public DeadState() 
    {
        stateID = FSMStateID.Dead;
    }

    public override void Reason(List<Transform> targets, Transform target, NPCTankController npc)
    {

    }

    public override void Act(List<Transform> targets, Transform target, NPCTankController npc)
    {
        //Do Nothing for the dead state
    }

	public override void DoBeforeEntering(NPCTankController npc)
	{

	}

	public override void DoBeforeLeaving(NPCTankController npc)
	{

	}
}
