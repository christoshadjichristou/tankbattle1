using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using System.Collections.Generic;

public class FSM : MonoBehaviour 
{
    public Transform currentTarget;
	protected List<Transform> targets = new List<Transform>();
	protected Vector3 destPos; 
	protected GameObject[] waypointList;
	public NavMeshAgent navmeshAgent { get; protected set; }

    //Bullet shooting rate
    protected float shootRate;
    protected float elapsedTime;

	[SerializeField]
	public Transform turret;// { get; set; }

	[SerializeField]
	public Transform bulletSpawnPoint;// { get; set; }

    protected virtual void Initialize() { }
    protected virtual void FSMUpdate() { }
    protected virtual void FSMFixedUpdate() { }

	void Start () 
    {
        Initialize();
	}
	
	void Update () 
    {
        FSMUpdate();
	}

    void FixedUpdate()
    {
        FSMFixedUpdate();
    }    
}
